﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private GameObject MenuUI;
    [SerializeField] private GameObject GameUI;

    [SerializeField] private Animator AnimBackground;

    [SerializeField] private GameController GameController;
    [SerializeField] private PlayerCharacter Player;

    [SerializeField] private Text TextMenu;

    [SerializeField] private Button GameStart;
    [SerializeField] private Text TextGameStart;

    static public bool IsPause = false;

    void Start()
    {
        TextMenu.text = "Game Labyrinth 3D";
        TextGameStart.text = "Новая игра";

        AnimBackground.Play("Background-outFade");

        GameUI.SetActive(false);
        MenuUI.SetActive(true);

        GameStart.onClick.RemoveAllListeners();
        GameStart.onClick.AddListener(NewGame);

        PlatformZone.EventGameOver += GameOver;
        PlatformZone.EventGameWin += GameWin;
    }

    void GameOver()
    {
        AnimBackground.Play("Background-outFade");

        TextMenu.text = "Вы проиграли!";
        TextGameStart.text = "Новая игра";

        GameUI.SetActive(false);
        MenuUI.SetActive(true);

        GameStart.onClick.RemoveAllListeners();
        GameStart.onClick.AddListener(NewGame);
    }

    void GameWin()
    {
        AnimBackground.Rebind();
        AnimBackground.Play("Background-Fade");
    }

    public void NewGame()
    {
        AnimBackground.Play("Background-inFade");

        GameUI.SetActive(true);
        MenuUI.SetActive(false);

        GameController.GameStart();
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void GamePause()
    {
        if(IsPause)
        {
            AnimBackground.Play("Background-inFade");

            GameUI.SetActive(true);
            MenuUI.SetActive(false);

            IsPause = !IsPause;
        }
        else
        {
            AnimBackground.Play("Background-outFade");

            GameUI.SetActive(false);
            MenuUI.SetActive(true);

            TextMenu.text = "Пауза";
            TextGameStart.text = "Продолжить";

            GameStart.onClick.RemoveAllListeners();
            GameStart.onClick.AddListener(GamePause);

            IsPause = !IsPause;
        }
    }
}
