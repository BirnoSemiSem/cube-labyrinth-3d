﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerCharacter : MonoBehaviour
{
    public NavMeshAgent _agent;

    public bool PlayerInSheald = false;
    private Coroutine CorShield;

    [SerializeField] private MeshRenderer _playerMesh;
    [SerializeField] private Material _playerNormal;
    [SerializeField] private Material _playerInSheald;
    // Start is called before the first frame update
    void Start()
    {
        //_agent.updateRotation = false;
        _playerMesh.material = _playerNormal;

        PlatformZone.EventGameOver += GameOver;
    }

    public void StartPlayer(Vector3 Point)
    {
        _agent.enabled = true;
        _agent.SetDestination(Point);
    }

    void Update()
    {
        if(PlayerInSheald)
        {
            if (_playerMesh.material != _playerInSheald)
                _playerMesh.material = _playerInSheald;
        }
        else
        {
            if (_playerMesh.material != _playerNormal)
                _playerMesh.material = _playerNormal;
        }

        if (_agent.enabled)
        {
            if (UI.IsPause)
                _agent.Stop();
            else
                _agent.Resume();
        }
    }

    void GameOver()
    {
        _agent.ResetPath();
    }


    public void ActivatedShieldClick()
    {
        PlayerInSheald = true;
        CorShield = StartCoroutine(ActiveShield());
    }

    public void ActivatedShieldExit()
    {
        PlayerInSheald = false;

        if (CorShield != null)
            CorShield = null;
    }

    IEnumerator ActiveShield()
    {
        yield return new WaitForSeconds(2f);

        PlayerInSheald = false;
    }
}
