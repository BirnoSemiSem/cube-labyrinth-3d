﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameController : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] private PlayerCharacter Player;
    [SerializeField] private GameObject StartZone;
    [SerializeField] private GameObject WinZone;

    [Header("Map")]
    [SerializeField] private Transform MapWallHeightX;
    [SerializeField] private Transform MapWallWidthZ;

    [Header("Wall")]
    [SerializeField] private GameObject[] PrefabWallObstacle;
    [SerializeField] private List<GameObject> ListWallsObstacle = new List<GameObject>();
    [SerializeField] private int MinRandomCountWall = 3;
    [SerializeField] private int MaxRandomCountWall = 5;
    [SerializeField] private int MinRandomScaleXWall = 3;
    [SerializeField] private int MaxRandomScaleXWall = 5;

    [Header("DeathZone")]
    [SerializeField] private GameObject[] PrefabDeathZone;
    [SerializeField] private List<GameObject> ListDeathZone = new List<GameObject>();
    [SerializeField] private int MinRandomCountZone = 3;
    [SerializeField] private int MaxRandomCountZone = 5;
    [SerializeField] private int MinRandomScaleXZone = 3;
    [SerializeField] private int MaxRandomScaleXZone = 5;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0, r = Random.Range(MinRandomCountWall, MaxRandomCountWall); i < r; i++)
            CreateStone();

        for (int i = 0, r = Random.Range(MinRandomCountZone, MaxRandomCountZone); i < r; i++)
            CreateDeathZone();

        PlatformZone.EventGameWin += RestartGame;
    }

    public void GameStart()
    {
        Player._agent.enabled = false;
        Player.transform.position = StartZone.transform.position;

        if (ListWallsObstacle.Count > 0)
        {
            for(int i = 0; i < ListWallsObstacle.Count; i++)
            {
                ListWallsObstacle[i].transform.position = Vector3.zero;
            }
        }

        if (ListDeathZone.Count > 0)
        {
            for (int i = 0; i < ListDeathZone.Count; i++)
            {
                ListDeathZone[i].transform.position = Vector3.zero;
            }
        }

        if (ListWallsObstacle.Count > 0)
        {
            for (int i = 0; i < ListWallsObstacle.Count; i++)
            {
                RandomSpawnElement(ListWallsObstacle[i], 1.01f, MinRandomScaleXWall, MaxRandomScaleXWall);
            }
        }

        if (ListDeathZone.Count > 0)
        {
            for (int i = 0; i < ListDeathZone.Count; i++)
            {
                RandomSpawnElement(ListDeathZone[i], 0.7f, MinRandomScaleXZone, MaxRandomScaleXZone);
            }
        }

        if (!IsInvoking(nameof(PlayerGo)))
            Invoke(nameof(PlayerGo), 2f);
    }

    void PlayerGo()
    {
        Player.transform.position = StartZone.transform.position;

        Player.StartPlayer(WinZone.transform.position);
    }

    public void RestartGame()
    {
        if (!IsInvoking(nameof(GameStart)))
            Invoke(nameof(GameStart), 1f);
    }

    private void CreateStone()
    {
        GameObject Wall = Instantiate(PrefabWallObstacle[Random.Range(0, PrefabWallObstacle.Length)]);
        ListWallsObstacle.Add(Wall);
    }

    private void CreateDeathZone()
    {
        GameObject DeathZone = Instantiate(PrefabDeathZone[Random.Range(0, PrefabDeathZone.Length)]);
        ListDeathZone.Add(DeathZone);
    }

    private void RandomSpawnElement(GameObject element, float SpawnY, int MinRandomScaleX, int MaxRandomScaleX)
    {
        bool HitDetected = true;

        int TempX = 0;
        int TempZ = 0;

        int RdmRotation = 0;
        int RdmScale = 0;

        Collider colliderElemet = element.GetComponent<Collider>();

        colliderElemet.enabled = false;

        int TryCount = 100;

        while (HitDetected && TryCount > 0)
        {
            RdmRotation = Random.Range(0, 10) > 5 ? 0 : 90;
            RdmScale = Random.Range(MinRandomScaleX, MaxRandomScaleX + 1);

            TempX = (int)Random.Range(-MapWallHeightX.position.x, MapWallHeightX.position.x);
            TempZ = (int)Random.Range(-MapWallWidthZ.position.z, MapWallWidthZ.position.z);

            element.transform.position = new Vector3(TempX, SpawnY, TempZ);
            element.transform.rotation = Quaternion.Euler(0, RdmRotation, 0);

            if(colliderElemet.isTrigger)
                element.transform.localScale = new Vector3(RdmScale, element.transform.localScale.y, RdmScale);
            else
                element.transform.localScale = new Vector3(RdmScale, element.transform.localScale.y, element.transform.localScale.z);

            HitDetected = Physics.CheckBox(colliderElemet.bounds.center, element.transform.localScale/2, Quaternion.Euler(0, RdmRotation, 0));

            TryCount--;
        }

        colliderElemet.enabled = true;
    }
}
