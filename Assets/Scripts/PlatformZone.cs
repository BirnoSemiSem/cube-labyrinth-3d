﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlatformZone : MonoBehaviour
{
    public enum Platform
    {
        Start,
        Win,
        Death
    }

    public Platform Zone;

    public static UnityAction EventGameWin;
    public static UnityAction EventGameOver;

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            if (Zone == Platform.Death)
            {
                if (!collider.gameObject.GetComponent<PlayerCharacter>().PlayerInSheald)
                    EventGameOver?.Invoke();
            }
            else if(Zone == Platform.Win)
            {
                EventGameWin?.Invoke();
            }
        }
    }
}
